﻿#include <iostream>

int main()
{
    // Вывести числа от 1 до 10
    /*std::cout << 1 << std::endl;
    std::cout << 2 << std::endl;
    // ...
    std::cout << 10 << std::endl;*/

    // while
    // i - итератор (счетчик)
    /*int i = 0;
    while (i < 10)
    {
        std::cout << i << std::endl;
        i++;
    }*/

    // do .. while

    /*int i = 0;
    do
    {
        std::cout << i << std::endl;
        i++;
    }
    while (i < 10);*/

    // for
    /*for (int i = 0; i < 10; i++)
        std::cout << i << std::endl;    // Тело цикла*/

    // ++i;
    /*int i = 0;
    int x = ++i;
    std::cout << x << std::endl;*/

    // Поиск максимального числа в последовательности
    // Дается 10 целых чисел, числа в диапазоне от -1000 до 1000, найти максимум

    // Поиск максимума
    /*int x;
    int max = INT_MIN;  // -1000
    for (int i = 0; i < 3; i++)
    {
        std::cin >> x;
        if (x > max)
            max = x;
    }
    std::cout << max << std::endl;*/

    // Поиск минимума
    /*int x;
    int min = INT_MAX;  // -1000
    for (int i = 0; i < 3; i++)
    {
        std::cin >> x;
        if (x < min)
            min = x;
    }
    std::cout << min << std::endl;*/

    // Область видимости переменной
    /*int i = 0;

    for (i = 0; i < 10; i++)
    {
        std::cout << "In cycle " << i << std::endl;
    }

    std::cout << "Out cycle " << i << std::endl;*/

    // Распаковка натурального числа. Дано число не больше 1000. 
    // 1. Вывести все его цифры
    // 2. Вывести сумму цифр

    /*int x;
    int sum = 0;
    std::cin >> x;

    while (x > 0)
    {
        int digit = x % 10;
        sum = sum + digit; // sum+=digit;
        std::cout << digit << std::endl;

        x = x / 10; // x/=10;
    }

    std::cout << sum << std::endl;*/

    // Ввести с клавиатуры N чисел, остановить цикл при вводе 0
    /*int x;
    while (true)
    {
        std::cin >> x;
        std::cout << "Get " << x << std::endl;

        if (x == 0)
            break;
    }*/

    //std::cout << "Cycle stopped" << std::endl;

    /*setlocale(LC_ALL, "Rus");

    // Определение простоты числа
    int x;
    std::cin >> x;

    bool flag = true;*/

    /*
        64
        2 32
        4 16
        8 8
        16 4
        32 2
    
    */

    /*for (int d = 2; d <= sqrt(x); d++)   // #include <math.h>
    {
        //std::cout << "d = " << d << std::endl;
        if (x % d == 0)
        {
            flag = false;
            break;
        }
    }

    if(flag)    // flag == true
        std::cout << "Простое" << std::endl;
    else
        std::cout << "Составное" << std::endl;*/

    /*int x;
    for (int i = 0; i < 10; i++)
    {
        std::cin >> x;
        if (x <= 0)
            continue;

        std::cout << x << std::endl;
        x = x + 10;
        x = x * 10;
    }*/


    /*for (int i = 0; i < 10; i++)
        for (int j = 0; j < 10; j++)
        {
            if (j > 5)
                continue;
            std::cout << i << "*" << j << "=" << i * j << std::endl;
        }*/

    // Есть 10 чисел, четные числа умножить на 2, нечетные пропустим
    /*int x;
    for (int i = 0; i < 10; i++)
    {
        std::cin >> x;

        if (x % 2 == 1)
            continue;

        std::cout << "x*2 = " << x * 2 << std::endl;
    }*/

    // Пример выхода из двух циклов через break
    for (int i = 0; i < 10; i++)
    {
        bool stop = false;
        for (int j = 0; j < 10; j++)
        {
            if (i * j > 50)
            {
                std::cout << i << " " << j << std::endl;
                stop = true;
                break;
            }
        }

        if (stop)
            break;
    }

}