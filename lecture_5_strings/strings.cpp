﻿#include <iostream>
#include <fstream>
#include <sstream>
#include <windows.h>

int main()
{
    /*
        1. Для русского текста создать файл и выставить кодировку Windows 1251 (Notepad++)
        2. setlocale(LC_ALL, "Rus");
        3. SetConsoleCP(1251);
    */

    setlocale(LC_ALL, "Rus");
    SetConsoleCP(1251);         // Установка кодировки для вывода в консоль

    std::ifstream in("input.txt");
    std::ofstream out("output.txt");

    // Символ
    /*char c, t;
    in >> c >> t;

    std::cout << c << " " << t << std::endl;
    out << c;*/

    // Посимвольный ввод
    // Посмотреть насчет считывания пробелов и переносов строки


    std::cout << '\a';
   /* char c;
    while (in.get(c))     // '\n', in >> c - пока можем прочитать
    {
        std::cout << '<' << c << ">!!!" << '\a' << (int)c << std::endl;
    }*/

    // Найти наиболее часто встречающийся символ в тексте
    /*int symbols[256];
    for (int i = 0; i < 256; i++)
        symbols[i] = 0;

    unsigned char c;
    while (in >> c)     // '\n', in >> c - пока можем прочитать
    {
        symbols[c]++;
        //std::cout << c << " " << (int)c << std::endl;
    }

    for (int i = 192; i < 256; i++)
    {
        std::cout << symbols[i] << " " << (char)i << std::endl;
    }*/

    /*// Верхний регистр ПРТС
    // Нижний регистр пртс
    int symbols[256];
    for (int i = 0; i < 256; i++)
        symbols[i] = 0;

    unsigned char c;
    while (in >> c)     // '\n', in >> c - пока можем прочитать
    {
        // Заглавная буква
        if (c >= 192 && c <= 223)
            c += 32;
        symbols[c]++;
        //std::cout << c << " " << (int)c << std::endl;
    }

    for (int i = 224; i < 256; i++)
    {
        std::cout << symbols[i] << " " << (char)i << " " << (int)i << std::endl;
    }*/

    // Строка
    /*char s[100], t[100];
    in >> s >> t;
    std::cout << s << " " << t;*/

    // Ввод по словам
    /*char s[100];
    while (!in.eof())
    {
        in >> s;
        std::cout << "<" << s << ">" << std::endl;
    }*/

    // Ввод построчный
    /*char s[100];

    while (!in.eof())
    {
        in.getline(s, 100);
        
        //std::cout << s << std::endl;

        // Проверка на наличие П в строке
        for (int i = 0; i < strlen(s); i++)
            if (s[i] == 'П')
                std::cout << "Есть П!!!!" << std::endl;

        // Замена строчных букв на заглавные
        for (int i = 0; i < strlen(s); i++)
            if (s[i] >= 'а' && s[i] <= 'я')
                s[i] -= 32;

        std::cout << s << std::endl;
    }*/


    // Объединение строк
    /*char s1[100] = "Привет, ";
    char s2[100] = "мир!";
    strcat_s(s1, s2);

    std::cout << s1 << " " << s2 << std::endl;*/

    // Сравнение строк
    /*char s3[100] = "ааба";
    char s4[100] = "аав";

    std::cout << strcmp(s3, s4) << std::endl;*/

    // Копирование строк
    /*char s1[100] = "";
    char s2[100] = "Привет, мир!";
    strcpy_s(s1, s2);

    std::cout << s1 << " | " << s2 << std::endl;*/

    // C++
    /*std::string s = "Привет, мир!";
    std::cout << s << " " << s.length() << " " << s[4] << std::endl;

    for(int i=0;i<s.length();i++)
        if (s[i] >= 'а' && s[i] <= 'я')
            s[i] -= 32;

    std::cout << s << " " << s.length() << " " << s[4] << std::endl;

    // Сравнение строк
    std::string s1 = "авв";
    std::string s2 = "авв";

    if (s1 < s2) {
        std::cout << s1 << " меньше " << s2 << std::endl;
    }
    else if (s1 == s2)
    {
        std::cout << s1 << " равно " << s2 << std::endl;
    }
    else
    {
        std::cout << s1 << " больше " << s2 << std::endl;
    }

    // Сложение строк
    std::cout << s1 + s2 << std::endl;

    // Найти элемент в строке
    std::cout << "find " << s.find("Р") << std::endl;

    for (int i = 0; i < s.length(); i++)
    {
        std::cout << (int)s[i] << " " << (int)'М' << std::endl;
        if (s[i] == 'М')
            std::cout << "Найден " << i << std::endl;
    }*/

    // Ввод для string
    /*std::string s;
    //std::getline(in, s);
    in >> s;
    std::cout << s << std::endl;*/
    
}