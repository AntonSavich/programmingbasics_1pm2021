#pragma once
#include <iostream>
#define PI acos(-1)

namespace mt
{
	// �������� ������
	struct Point
	{
		// ��� ������� (public)
		int x;
		int y;
	};

	// �����
	class Circle
	{
		// ��� ������� (private)
	public:
		// �����������.
		// �� ����� ����.
		// ������ ���������� �����, ��� � �����
		// ����������� ���������� ������ ��� �������� �������
		// ����������� �� ��������� ������ ���� � ������
		Circle();
		Circle(Point M, int R);
		// �� ����� ����
		// ��� ��������� � ��������� ������ + ~ � ������
		// ���������� ���������� ������ ��� ����������� �������
		// ���������� �� ���������
		~Circle();

		double Square();
		void SetR(int R);
		int GetR() { return m_R; }
		void SetM(Point M);

		// �� ������� ��� ���������� �������!!!
	private:
		Point m_M;
		int m_R;
	};
}